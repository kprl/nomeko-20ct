<?php
/**
 * Displays the header blocks
 */

?>
<div class="header_blocks">

	<div class="container-fluid">
		<div class="row no-gutters">

			<div class="col-sm">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/block_svets.png" class="img-fluid" alt="Responsive image">
				<a href="<?php echo get_home_url(); ?>/svetsning/" id="svets" class="overlay">
					<h3>Svetsning</h3>
	        <p class="text">Vi erbjuder både manuell och robotsvetsning. Vårt samarbete med de övriga bolagen inom Hermano koncernen, som är certifierade enligt ISO 3834-2, ger våra kunder en hög kompetens och en stabil produktion.</p>
        </a>
			</div>

			<div class="col-sm">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/block_robot.png" class="img-fluid" alt="Responsive image">
				<a href="<?php echo get_home_url(); ?>/bearbetning/" id="svarv" class="overlay">
					<h3>Bearbetning</h3>
	        <p class="text">Vi är vana att jobba med stora serier och mindre styck tillverkning, små toleranser är inte heller något problem. Viktgräns är 10 ton.</p>
        </a>
			</div>

			<div class="col-sm">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/block_figurskarning.png" class="img-fluid" alt="Responsive image">
				<a href="<?php echo get_home_url(); ?>/figurskarning/" id="robot" class="overlay">
					<h3>Figurskärning</h3>
	        <p class="text">Vi har jobbat med figurskärning i plåt sedan 1957. Idag har vi två gasmaskiner och en finplasma. Vi hanterar plåt i tjocklekar mellan 10mm – 300mm, lyftkapacitet på 12 ton.</p>
        </a>
			</div>

		</div>
	</div>

</div>
