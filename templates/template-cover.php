<?php
/**
 * Template Name: Frontpage
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0
 */

get_header();
?>

<main id="site-content" role="main">

	<?php

	$meta = get_post_meta( get_the_ID() );

	?>

	<div class="frontpage-cover-block container-fluid">

		<div class="row">

			<div class="col-12 col-md-6">
				<div class="frontpage-valkomsttext">
					<?php
						$linken = unserialize($meta['frontpage_lank'][0]);
						echo wpautop($meta['frontpage_valkomsttext'][0]);
						echo "<a href='" . $linken['url'] . "'>" . $meta['frontpage_lanktext'][0] . "</a>";
					?>
				</div>
			</div>

			<div class="frontpage-logotyp d-none d-md-block col-12 col-md-6">
				<?php
					echo wp_get_attachment_image( $meta['frontpage_logotyp'][0], 'full', "", ["class" => "mx-auto"] );
				?>
			</div>

		</div>

	</div>

	<?php

	if ( have_posts() ) {

		while ( have_posts() ) {
			the_post();

			get_template_part( 'template-parts/content-cover' );
		}
	}

	?>

</main><!-- #site-content -->

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php get_footer(); ?>
